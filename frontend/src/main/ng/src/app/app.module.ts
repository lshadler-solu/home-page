/**
* @copyright 2017: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/

// Angular modules and components
import { NgModule, ApplicationRef, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



// Time Reporting components
import { routing } from './views/app.routing';
import { ViewsModule } from './views/module';

// Core Solu FE module
import { SoluCoreLibraryModule } from '../solu-core/frontend/lib/solu-core.module';
// import services used in the App Initializer
import { AppComponent, AppService } from '../solu-core/frontend/lib/solu-core.module';
import { AuthenticationService, checkAuthentication } from '../solu-core/frontend/lib/solu-core.module';

import { LogoutComponent } from '../solu-core/frontend/lib/layout/logout.component';
/**
 * Declaration of the Time Reporting (TR) micro-service application module.
 * 
 * @author Bryan Basham <bbasham@solutechnology.com>
 * 
 * @class
 */
@NgModule({

  declarations: [
  ],

  entryComponents: [
    // TBD
    LogoutComponent
  ],

  imports: [
    // generic modules
    BrowserModule
    , FormsModule
    , ReactiveFormsModule
    , HttpModule
    , BrowserAnimationsModule
    // Solu core modules
    , SoluCoreLibraryModule
    // local modules
    , routing
    , ViewsModule
  ],
  providers: [
    // Perform user authentication
    {
      provide: APP_INITIALIZER,
      useFactory: checkAuthentication,
      multi: true,
      deps: [AppService, AuthenticationService]
    }
  ],  // end of providers
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
