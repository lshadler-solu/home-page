/**
 * Copyright 2015 - 2017: Solu Technology Partners
 * All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
 * Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
 */
package com.solu;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.solu.util.SoluLogger;

/**
 * The micro-service application configuration.
 * 
 * @author <a href='mailto:basham47@gmail.com'>Bryan Basham</a> 
 */
@SpringBootApplication
public class MicroServiceApplication extends SpringBootServletInitializer {
  private static final SoluLogger LOG = SoluLogger.getLogger(MicroServiceApplication.class);

  public static void main(String[] args) {
    SpringApplication.run(MicroServiceApplication.class, args);
  }

  @Autowired
  private Environment environment;

  /**
   * Configure the application with additional configuration components.
   * {@inheritDoc}
   */
  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    LOG.logDebug("Configuring the %s environment.",
        (environment == null) ? "UNKNOWN" : Arrays.toString(environment.getActiveProfiles()));
    return application.sources(MicroServiceApplication.class
        // basic backend configuration
        ,SoluCoreConfig.class
        );
  }

  @Bean
  public javax.validation.Validator localValidatorFactoryBean() {
     return new LocalValidatorFactoryBean();
  }
}
