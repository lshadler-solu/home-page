/**
* Copyright 2015 - 2015: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.solu.util.SoluLogger;

/**
 * The main webapp controller that handles login, index and other core pages.
 * Because the micro-service is an Angular app, there are very few HTTP pages to
 * handle and these don't normally require data to be injected using the
 * {@link ModelAndView} Spring MVC framework.  Rather these methods just need
 * to return the name of the Thymeleaf view to render.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
@Controller
public class MainController {
  private static final SoluLogger LOG = SoluLogger.getLogger(MainController.class);

  public static final String HOME_VIEW = "index";
  public static final String ERROR_VIEW = "error";
  public static final String HOME_URL = "/";
  public static final String INDEX_URL = "/index.html";
  public static final String REDIRECT_HOME = "redirect:" + HOME_URL;

  @Value("${solu.web.context.path}")
  private String webappContextPath;

  //
  // REST services
  //

  /**
   * The webapp's Home page handler.
   * 
   * <p>
   * The Thymeleaf view contains the SPA HTML page for the Angular app.
   * 
   * @return the symbolic name of the view to render
   */
  @RequestMapping(value = { INDEX_URL, HOME_URL }, method = RequestMethod.GET)
  public String homePage(final Model model, final HttpServletRequest request) {   
    LOG.logDebug("Home page '%s'", webappContextPath);
    // TODO: each micro-service app will need a version
//    model.addAttribute("appVersion", Application.getServerConfig().appVersion);
    model.addAttribute("ctxPath", webappContextPath);
    return HOME_VIEW;
  }

}
