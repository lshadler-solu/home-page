/**
 * Copyright 2015 - 2017: Solu Technology Partners
 * All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
 * Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
 */
package com.solu;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

import com.solu.util.SoluLogger;

/**
 * The development-mode app.
 * 
 * @author <a href='mailto:basham47@gmail.com'>Bryan Basham</a> 
 */
@SpringBootApplication
@EnableAutoConfiguration
@Profile(SoluProfile.DEV)
public class DevelopmentApplication extends SpringBootServletInitializer {
  private static final SoluLogger LOG = SoluLogger.getLogger(DevelopmentApplication.class);

  public static void main(String[] args) {
    SpringApplication.run(DevelopmentApplication.class, args);
  }

  @Autowired
  private Environment environment;
  
  /**
   * Configure the application with additional configuration components.
   * {@inheritDoc}
   */
  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    LOG.logDebug("Configuring the %s environment.",
        (environment == null) ? "UNKNOWN" : Arrays.toString(environment.getActiveProfiles()));
      return application.sources(DevelopmentApplication.class
          // basic backend configuration
          ,SoluCoreConfig.class
          );
  }
}
