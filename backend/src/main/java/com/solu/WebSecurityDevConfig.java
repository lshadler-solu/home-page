/**
 * Copyright 2015 - 2017: Solu Technology Partners
 * All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
 * Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
 */
package com.solu;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.solu.security.jwt.JwtAuthenticationTokenFilter;
import com.solu.util.SoluLogger;

/**
 * Web security when running the micro-service backend during development.
 * 
 * <p>
 * This configuration is only used when the Spring profile is dev.
 * 
 * @author <a href='mailto:basham47@gmail.com'>Bryan Basham</a> 
 */
@Configuration
@Profile(SoluProfile.DEV)
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebSecurityDevConfig extends WebSecurityConfigurerAdapter {
  private static final SoluLogger LOG = SoluLogger.getLogger(WebSecurityConfig.class);

  @Autowired
  private Environment environment;
  
  @Override
  protected void configure(HttpSecurity httpSecurity) throws Exception {
    LOG.logDebug("Configuring the %s environment.", Arrays.toString(environment.getActiveProfiles()));
    httpSecurity
        // we don't need CSRF because our token is invulnerable
        .csrf().disable()
        // all /api/** calls must be authenticated
        .authorizeRequests().antMatchers("/api/**").authenticated()
        // and we're using a JWT filter to perform authentication
        .and()
        .addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);

    // disable page caching
    httpSecurity.headers().cacheControl();
  }

  @Bean
  public JwtAuthenticationTokenFilter authenticationTokenFilterBean() throws Exception {
      return new JwtAuthenticationTokenFilter();
  }
}